//
//  ViewController.m
//  CollectionView
//
//  Created by click labs 115 on 10/1/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *collectionData;
}
@property (strong, nonatomic) IBOutlet UICollectionView *viewCollection;

@end

@implementation ViewController
@synthesize viewCollection;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    collectionData = [NSMutableArray new];

    for (int i=1; i<100; i++){
        NSString *strCollectionData = [NSString stringWithFormat:@"%d", i ];
        
    //for (NSNumber *i = 0; i < [NSNumber numberWithInteger:100]; i++)
    
        [collectionData addObject:strCollectionData];
    

   

    // Do any additional setup after loading the view, typically from a nib.
}
     [viewCollection reloadData];
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  2;
}
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return collectionData.count;
}
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseCollection" forIndexPath:indexPath];
  UILabel *lblcollection =(UILabel*) [cell viewWithTag:100];
    
    lblcollection.text = collectionData [indexPath.row];
    
    return cell;
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
