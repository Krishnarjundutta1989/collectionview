//
//  ViewController.h
//  CollectionView
//
//  Created by click labs 115 on 10/1/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
<UICollectionViewDelegate,UICollectionViewDataSource>


@end

